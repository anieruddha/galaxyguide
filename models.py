
class Item(object):
    def __init__(self,key,val):
        self.key = key
        self.value = val

    def getValue(self):
        return self.value

    def __repr__(self):
        return self.key

    def __str__(self):
        return self.key

    def __hash__(self):
        return len(self.value)

    def __eq__(self, other):
        return self.key == other.key


