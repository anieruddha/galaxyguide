Problem Statement : Merchant's Guide to the Galaxy

Programming Lanaguage : Python 2.7.3.

How to Run Code :
Assuming Python 2.7.3 installed & availble on Path
To Run Application : $python app.py ./data/test_input.txt 
To Run Test Cases : python -m unittest discover

Assumption :
1. One input statement can contain only one metal e.g. glob prok Gold is valid glob prok Gold Silver is invalid
2. In One input statement, if metal is mentioned, it will be only followed by units e.g. glob glob Silver is valid, but
glob Silver glob is invalid
3. Input statements assumed to follow strict pattern & case sensitive.
4. Program need absolute or relative file path as argument. Path separator is os dependent

Design Decisions :
app.py : This module is starting point of application

config.py : This module contains data types which can be configurable to change behaviour. With current implementation, they been
used in implementing rules.

file_reader.py : This module read the input file & return list of lines. This list of lines will be then processed

processor.py : This module initialize Processor object with list of lines.  These lines then parsed & model objects created
 from that.  RegEx expression used to identify given string is information, question or can be model.

models.py : This module contain defination for model object. Statement like 'glob is I' converted into model object something
like {'key':'glob','value':'I'}. config.py module contain the decimal value for given roman literal.

rules.py : module contain the rules implementation.
For Rule : 'Only one small-value symbol may be subtracted from any large-value symbol.', with current code design, implementation
node needed




