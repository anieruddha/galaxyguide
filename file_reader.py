
class FileReader(object):

    def __init__(self, filePath):
        self.filePath = filePath

    def read(self):
        file = open(self.filePath,mode='rb')
        lines = [line.rstrip('\n') for line in file]
        return lines
