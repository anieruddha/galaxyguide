import unittest
from rules import Rules
from processor import Processor
from custom_exceptions import RuleVoilationException


class TestRules(unittest.TestCase):

    def test_invalid_repeat(self):
        str = 'IIIIV'
        with self.assertRaises(RuleVoilationException):
            Rules.is_valid_repeat(str)

    def test_valid_repeat(self):
        str = 'Q'
        self.assertTrue(Rules.is_valid_repeat(str))

    def test_invalid_substract(self):
        str = 'IC'
        with self.assertRaises(RuleVoilationException):
            Rules.is_valid_substract(str)

    def test_valid_substract(self):
        str = 'IV'
        self.assertTrue(Rules.is_valid_substract(str))

    def test_valid_roman(self):
        str = 'XV'
        self.assertTrue(Rules.is_valid(str))

    def test_invalid_literal(self):
        str = 'IQV'
        with self.assertRaises(RuleVoilationException):
            Rules.is_valid(str)


class TestProcessor(unittest.TestCase):

    def setUp(self):
        data = [
            'glob is X',
            'prok is V',
            'glob glob Silver is 34 Credits',
            'how much is glob glob ?',
            'how much is prok glob ?',
            'how many Credits is glob prok Silver ?',
        ]
        self.processor = Processor(data)

    def test_process(self):
        msgs = self.processor.process()
        self.assertIn('glob glob is 20', msgs)
        self.assertIn('prok glob is 5', msgs)
        self.assertIn('glob prok Silver is 68 Credits', msgs)



if __name__ == '__main__':
    suite = unittest.TestSuite((
        unittest.TestLoader().loadestsFromTestCase(TestRules),
        unittest.TestLoader().loadestsFromTestCase(TestProcessor),
    ))
    unittest.TextTestRunner(verbosity=2).run(suite)