

class GalaxyGuideException(Exception):
    def __init__(self):
        self.message = 'Generic Galaxy Guide Exception'

    def __str__(self):
        return self.message

    def __repr__(self):
        return self.message


class RuleVoilationException(GalaxyGuideException):
    def __init__(self):
        self.message = 'Roman numeric string invalid'


class WrongInputException(GalaxyGuideException):
    def __init__(self):
        self.message = 'Invalid input provided'





