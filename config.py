
ROMAN_ENUMS = ['I','V','X','L','C','D','M']

DECIMAL_VALUE = {
    'I':1,
    'V':5,
    'X':10,
    'L':50,
    'C':100,
    'D':500,
    'M':1000
}

ALLOWED_SUBTRACTION = {
    'I':('V','X'),
    'X':('L','C'),
    'C':('D','M'),
    'V': ('X',)
}

ALLOWED_MAX_REPEAT={
    'I':3,
    'X':3,
    'C':3,
    'M':3
}