import config
from custom_exceptions import RuleVoilationException


class Rules(object):
    @classmethod
    def is_valid_repeat(cls, romanStr):
        tmp = romanStr
        for c in tmp:
            num = config.ALLOWED_MAX_REPEAT.get(c,1) + 1
            if (c*num) in romanStr:
                raise RuleVoilationException
        return True

    @classmethod
    def is_valid_substract(cls,romanStr):
        counter = 0
        tmp = romanStr
        for c in romanStr[:-1]:
            valid = config.ALLOWED_SUBTRACTION.get(c)
            counter += 1

            if tmp[counter] == None or tmp[counter] == c:
                continue

            if valid and tmp[counter] not in valid:
                raise RuleVoilationException
        return True

    @classmethod
    def is_valid_literal(cls,romanStr):
        for c in romanStr:
            if not c in config.ROMAN_ENUMS:
                raise RuleVoilationException
        return True

    @classmethod
    def is_subtraction_needed(cls, romamStr):
        if len(romamStr) ==2:
            x = config.DECIMAL_VALUE[romamStr[0]]
            y = config.DECIMAL_VALUE[romamStr[1]]
            return x < y
        return True

    @classmethod
    def is_valid(cls,romanStr):
        result =  (cls.is_valid_literal(romanStr) and cls.is_valid_repeat(romanStr))

        if(result and cls.is_subtraction_needed(romanStr)):
            return cls.is_valid_substract(romanStr)

        return result
