from config import DECIMAL_VALUE as values

def roman_to_decimal(strRoman):
    numbers = []
    for char in strRoman:
        numbers.append(values[char])
    total = 0
    for num1, num2 in zip(numbers, numbers[1:]):
        if num1 >= num2:
            total += num1
        else:
            total -= num1
    return total + num2