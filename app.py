import sys
from file_reader import FileReader
from processor import Processor

def main(argv):
    if len(argv) != 1:
        print("Please provide input file path")
        return -1

    path = argv[0]
    reader = FileReader(path)
    data = reader.read()
    processor = Processor(data)
    output = processor.process()
    print '\n'.join(output)

if __name__ == '__main__':
    main(sys.argv[1:])
