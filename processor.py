import re
from models import Item
from rules import Rules
from utils import roman_to_decimal
from custom_exceptions import WrongInputException

class Processor(object):

    MODEL_REGEX = r'(\w+) is (\w)'
    INFO_REGEX = r'(.+) is (\d+) Credits'
    Q_REGEX = r'how much is (.+)\s+\?'
    Q_CRED_REGEX = r'how many Credits is (.+)\s+\?'
    ERROR_MSG = 'I have no idea what you are talking about'
    SUCCESS_MSG_1 =  '{0} is {1:.0f}'
    SUCCESS_MSG_2 = '{0}  is {0:.0f} Credits'

    def __init__(self,lstData):
        if not isinstance(lstData,list):
            raise WrongInputException

        self.raw = lstData
        self.items = {}
        self.questions = []
        self.information = []
        self.metals = {}

    def __process_info(self,strData):
        romanStr = ''
        match = re.search(self.INFO_REGEX, strData)
        tmp = match.group(1).split(' ')
        for i in tmp[:-1]:
            literal = self.items.get(i)
            if not literal:
                raise WrongInputException

            romanStr += literal.getValue()

        Rules.is_valid(romanStr)

        metal = tmp[-1]
        amt = float(match.group(2))
        divider = roman_to_decimal(romanStr)
        unitprice = amt / divider
        self.metals[metal] = unitprice

    def __process_line(self,strData):
        if(re.match(self.MODEL_REGEX,strData)):
            match = re.search(self.MODEL_REGEX,strData)
            item = Item(match.group(1), match.group(2))
            self.items[match.group(1)] = item
        elif(re.search(self.INFO_REGEX,strData)):
            self.information.append(strData)
        else:
            self.questions.append(strData)

    def __process_question(self,str):
        msg = ''
        pattern = re.match(self.Q_REGEX, str)
        if pattern:
            msg = pattern.group(1) + ' is {0:.0f}'
        else:
            pattern = re.match(self.Q_CRED_REGEX, str)
            if pattern:
                msg = pattern.group(1) + ' is {0:.0f} Credits'

        if not msg:
            return self.ERROR_MSG

        romanStr = ''
        items = pattern.group(1).split()
        for i in items[:-1]:
            tmp = self.items.get(i)
            if tmp:
                romanStr += tmp.getValue()
            else:
                raise WrongInputException

        tmp = items[-1]
        item = self.items.get(tmp)
        if item:
            romanStr += item.getValue()
            Rules.is_valid(romanStr)
            return msg.format(roman_to_decimal(romanStr))
        else:
            Rules.is_valid(romanStr)
            item = self.metals.get(tmp)
            if item:
                return msg.format(roman_to_decimal(romanStr) * item)
        return self.ERROR_MSG

    def process(self):
        msg_list = []
        for line in self.raw:
            self.__process_line(line)

        for line in self.information:
            self.__process_info(line)

        for line in self.questions:
            msg_list.append(self.__process_question(line))

        return msg_list

